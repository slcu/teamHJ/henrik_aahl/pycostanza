==========
pyCostanza
==========


.. image:: https://img.shields.io/pypi/v/pycostanza.svg
        :target: https://pypi.python.org/pypi/pycostanza

.. image:: https://img.shields.io/travis/supersubscript/pycostanza.svg
        :target: https://travis-ci.org/supersubscript/pycostanza

.. image:: https://readthedocs.org/projects/pycostanza/badge/?version=latest
        :target: https://pycostanza.readthedocs.io/en/latest/?badge=latest
        :alt: Documentation Status




PyCostanza is a Python re-write of the ImageJ tool Costanza. It includes
features for segmenting objects in images (e.g. cell nuclei or whole cells)
based on an intensity gradient. 


* Free software: GNU General Public License v3
* Documentation: https://pycostanza.readthedocs.io.


Features
--------

* TODO 

Credits
-------

This package was created with Cookiecutter_ and the `audreyr/cookiecutter-pypackage`_ project template.

.. _Cookiecutter: https://github.com/audreyr/cookiecutter
.. _`audreyr/cookiecutter-pypackage`: https://github.com/audreyr/cookiecutter-pypackage
